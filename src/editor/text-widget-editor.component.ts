import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { TextWidgetEditorResult } from './value/text-widget-editor-result';
import { TextWidgetEditorCreateSubject } from './value/text-widget-editor-create-subject';
import { TextWidgetEditorInterface } from './text-widget-editor-interface';

declare const Quill: any;

/**
 * 텍스트 위젯 컴포넌트
 *
 * @see {@link https://github.com/quilljs/quill}
 * @see {@link https://quilljs.com/}
 */
@Component({
  selector: 'text-widget-editor-component',
  template: '<div></div>',
  styleUrls: ['./text-widget-editor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TextWidgetEditorComponent implements OnInit, OnDestroy, TextWidgetEditorInterface {
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  // 컴포넌트에서 에디터가 처음 생성 되었는지 검사하기 위한 값
  private isFirstCreated: boolean = false;
  
  // Quill 오브젝트
  private quill: any;
  
  // Quill 에디터 엘리먼트
  private editorElement: HTMLElement;
  
  // 컨텐츠 ( 텍스트 )
  private content: string;
  
  // 컨텐츠 변경 알림
  private changeContentSubject$ = new Subject<string>();
  
  // 컨텐츠 변경 알림 구독
  private changeContentSubjectSubscription: Subscription;
  
  // 에디터 생성 알림
  private editorCreatedSubject$ = new Subject<TextWidgetEditorCreateSubject>();
  
  // 에디터 생성 알림 구독
  private editorCreatedSubjectSubscription: Subscription;
  
  // 에디터 값이 비어있는 경우 HTML 기본 값
  private EMPTY_HTML: string = '<p><br></p>';
  
  //----------------------------------------
  // Constant
  //----------------------------------------
  
  // 툴바 엘리먼트 클래스 이름
  // Quill 라이브러리가 만드는 툴바 영역의 클래스 이름이다
  private TOOLBAR_CLASS_NAME: string = '.ql-toolbar.ql-snow';
  
  // 타임 아웃에서 사용할 값
  private TIMEOUT_TIME_0_MS: number = 0;
  
  // 타임 아웃에서 사용할 값
  private TIMEOUT_TIME_1_MS: number = 1;
  
  // 타임 아웃에서 사용할 값
  private TIMEOUT_TIME_200_MS: number = 200;
  
  // 타임 아웃에서 사용할 값
  private TIMEOUT_TIME_500_MS: number = 500;
  
  // ---------------------------------
  // Default values
  // ---------------------------------
  
  // 기본 placeholder value
  private DEFAULT_PLACEHOLDER: string = 'Insert text here ...';
  
  // 기본 content(text) value
  private DEFAULT_CONTENT: string = '';
  
  // 기본 theme value
  private DEFAULT_THEME: string = 'snow';
  
  // 기본 readOnly value
  private DEFAULT_READ_ONLY: boolean = false;
  
  // 기본 formats value
  private DEFAULT_FORMATS: string[] = null;
  
  /**
   * 툴바 모듈 기본 값
   *  - 사용할 수 있는 툴바 목록 정의
   *
   *  e.g ) 아래 오브젝트는 사용할 수 있는 전체 툴바 모둘 값이다
   *      {
   *       toolbar: [
   *         ['bold', 'italic', 'underline', 'strike'],
   *         ['blockquote', 'code-block'],
   *         [
   *           {'header': 1},
   *           {'header': 2}
   *         ],
   *         [
   *           {'list': 'ordered'},
   *           {'list': 'bullet'}
   *         ],
   *         [
   *           {'script': 'sub'},
   *           {'script': 'super'}
   *         ],
   *         [
   *           {'indent': '-1'},
   *           {'indent': '+1'}
   *         ],
   *         [
   *           {'direction': 'rtl'}
   *         ],
   *         [
   *           {'size': ['small', false, 'large', 'huge']}
   *         ],
   *         [
   *           {'header': [1, 2, 3, 4, 5, 6, false]}
   *         ],
   *         [
   *           {'color': []},
   *           {'background': []}
   *         ],
   *         [
   *           {'font': []}
   *         ],
   *         [
   *           {'align': []}
   *         ],
   *         ['clean'],
   *         ['link', 'image', 'video']
   *       ]
   *     }
   *
   */
  private DEFAULT_MODULES: object = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [
        { header: 1 },
        { header: 2 }
      ],
      [
        { color: [] }
      ],
      [
        { header: [1, 2, 3, 4, 5, 6, false] }
      ],
      [
        { size: ['small', false, 'large', 'huge'] }
      ],
      [
        { font: [] }
      ]
    ]
  };
  
  // ---------------------------------
  // 알림 이벤트
  // ---------------------------------
  
  // 에디터 생성 완료 시 알림
  @Output()
  private onEditorCreated: EventEmitter<any> = new EventEmitter();
  
  // 에디터 컨텐츠(텍스트) 변경 시 알림
  @Output()
  private onContentChanged: EventEmitter<TextWidgetEditorResult> = new EventEmitter();
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  // ---------------------------------
  // Input List
  // ---------------------------------
  
  // 모듈
  @Input()
  public modules: Object;
  
  // ReadOnly
  @Input()
  public readOnly: boolean;
  
  // Placeholder
  @Input()
  public placeholder: string;
  
  // Fixed theme
  public theme: string;

  // Fixed formats
  public formats: string[];
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Constructor
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /**
   * 생성자
   * @param {ElementRef} element
   */
  constructor(private element: ElementRef) {
    
    // 텍스트 변경 시 알림
    this.contentChangedNotification();
    
    // isEditorCreatedSubject 처리
    this.editorCreatedSubjectSubscribe();
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Override Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /**
   * Init
   */
  public ngOnInit(): void {
    
    // 에디터 엘리먼트 저장
    this.editorElement = this.element.nativeElement.children[0];
    
    // 처음 생성하는 경우에 isFirstCreated 값을 TRUE 로 변경
    this.isFirstCreated = true;
    
    // 기본 값을 이용해서 초기화 실행
    this.initialize(
      this.content,
      this.modules,
      this.placeholder,
      this.readOnly
    );
  }
  
  /**
   * Destroy
   */
  public ngOnDestroy(): void {
    
    if (this.changeContentSubjectSubscription) {
      this.changeContentSubjectSubscription.unsubscribe();
    }
    
    if (this.editorCreatedSubjectSubscription) {
      this.editorCreatedSubjectSubscription.unsubscribe();
    }
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /**
   * 에디터 초기화
   *  - 에디터 초기화 완료 후 onEditorCreated Output 알림 이벤트 발생
   *
   * @param {string} content(text)
   *  e.g) A B C D E F G ...
   * @param {Object} modules
   *  e.g)
   *       {
   *       toolbar: [
   *         ['bold', 'italic', 'underline', 'strike'],
   *         ['blockquote', 'code-block'],
   *         [
   *           { 'header': 1 },
   *           { 'header': 2 }
   *         ],
   *         [
   *           { 'list': 'ordered' },
   *           { 'list': 'bullet' }
   *         ],
   *         [
   *           { 'script': 'sub' },
   *           { 'script': 'super' }
   *         ],
   *         [
   *           { 'indent': '-1' },
   *           { 'indent': '+1' }
   *         ],
   *         [
   *           { 'direction': 'rtl' }
   *         ],
   *         [
   *           { 'size': ['small', false, 'large', 'huge'] }
   *         ],
   *         [
   *           { 'header': [1, 2, 3, 4, 5, 6, false] }
   *         ],
   *         [
   *           { 'color': [] },
   *           { 'background': [] }
   *         ],
   *         [
   *           { 'font': [] }
   *         ],
   *         [
   *           { 'align': [] }
   *         ],
   *         ['clean'],
   *         ['link', 'image', 'video']
   *       ]
   *     }
   * @param {string} placeholder
   *  e.g) 'Insert text here ...'
   * @param {boolean} readOnly
   *  e.g) true or false value
   */
  public initialize(content: string = this.DEFAULT_CONTENT,
                    modules: object = this.DEFAULT_MODULES,
                    placeholder: string = this.DEFAULT_PLACEHOLDER,
                    readOnly: boolean = this.DEFAULT_READ_ONLY): void {
    
    if (this.isFirstCreated) {
      this.editorCreatedSubjectNext(content, modules, placeholder, readOnly);
    } else {
      setTimeout(() => {
        this.editorCreatedSubjectNext(content, modules, placeholder, readOnly)
      }, this.TIMEOUT_TIME_1_MS);
    }
  }
  
  /**
   * 텍스트 넣기
   * @param {string} text
   */
  public setContent(text: string): void {
    this.writeValue(text);
  }
  
  /**
   * 텍스트 가져오기
   * @returns {TextWidgetEditorResult}
   */
  public getContent(): TextWidgetEditorResult {
    
    let html = this.editorElement.children[0].innerHTML;
    const text = this.quill.getText();
    
    if (this.isEditorHtmlEmpty(html)) {
      html = null;
    }
    
    this.onModelChange(html);
    
    const textWidgetEditorResult: TextWidgetEditorResult = new TextWidgetEditorResult();
    textWidgetEditorResult.editor = this.quill;
    textWidgetEditorResult.html = html;
    textWidgetEditorResult.text = text;
    
    return textWidgetEditorResult;
  }
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
  
  /**
   * isEditorCreatedSubject 알림 발생
   * @param {string} content
   * @param {Object} modules
   * @param {string} placeholder
   * @param {boolean} readOnly
   */
  private editorCreatedSubjectNext(content: string, modules: Object, placeholder: string, readOnly: boolean): void {
    
    const textWidgetEditorCreateSubject: TextWidgetEditorCreateSubject = new TextWidgetEditorCreateSubject();
    textWidgetEditorCreateSubject.content = content;
    textWidgetEditorCreateSubject.modules = modules;
    textWidgetEditorCreateSubject.placeholder = placeholder;
    textWidgetEditorCreateSubject.readOnly = readOnly;
    this.editorCreatedSubject$.next(textWidgetEditorCreateSubject);
  }
  
  /**
   * isEditorCreatedSubject 구독 처리
   */
  private editorCreatedSubjectSubscribe(): void {
    
    // 약간에 시간차도 없이 초기화를 호출하는 경우 마지막 초기화 함수 호출에 대해서만 처리
    this.editorCreatedSubjectSubscription = this.editorCreatedSubject$
      .debounceTime(this.TIMEOUT_TIME_0_MS)
      .distinctUntilChanged()
      .switchMap(editor => Observable.of<any>(editor))
      .subscribe((editor) => {
        if (this.isEditorUndefined()) {
          // 에디터가 생성되지 않았는데 초기화를 호출하는 경우 500 ms 타임아웃 발생
          setTimeout(() => {
            this.create(editor.content, editor.modules, editor.placeholder, editor.readOnly);
          }, this.TIMEOUT_TIME_500_MS);
        } else {
          this.create(editor.content, editor.modules, editor.placeholder, editor.readOnly);
        }
      });
  }
  
  /**
   * 에디터 생성
   * @param {string} content
   * @param {Object} modules
   * @param {string} placeholder
   * @param {boolean} readOnly
   */
  private create(content: string, modules: Object, placeholder: string, readOnly: boolean): void {
    
    // 툴바가 있으면 삭제
    this.hasToolbarIfRemove();
    
    // 초기값 세팅
    this.content = content;
    this.modules = modules;
    this.placeholder = placeholder;
    this.readOnly = readOnly;
    
    // Fixed theme, formats
    this.theme = this.DEFAULT_THEME;
    this.formats = this.DEFAULT_FORMATS;
    
    // 에디터 생성
    this.quill = new Quill(
      this.editorElement, {
        modules: this.modules || this.DEFAULT_MODULES,
        placeholder: this.placeholder || this.DEFAULT_PLACEHOLDER,
        readOnly: this.readOnly || this.DEFAULT_READ_ONLY,
        theme: this.theme || this.DEFAULT_THEME,
        formats: this.formats || this.DEFAULT_FORMATS,
        boundary: document.body
      }
    );
    
    // 컨텐츠 값 세팅
    this.quill.pasteHTML(this.content);
    
    // 에디터 생성 완료 알림
    this.onEditorCreated.emit(this.quill);
    
    // 에디터 컴포넌트를 제외한 곳에 선택 이벤트가 발생하는 경우
    this.quill.on('selection-change', (range) => {
      if (!range) {
        this.onModelTouched();
      }
    });
    
    // 텍스트가 변경되면 모델 업데이트
    // noinspection JSUnusedLocalSymbols
    this.quill.on('text-change', (delta, oldDelta, source) => {
      const text = this.quill.getText();
      this.changeContentSubject$.next(text);
    });
  }
  
  /**
   * 텍스트 변경 시 알림
   */
  private contentChangedNotification(): void {
    
    this.changeContentSubjectSubscription = this.changeContentSubject$
      .debounceTime(this.TIMEOUT_TIME_200_MS)
      .distinctUntilChanged()
      .switchMap(text => Observable.of<string>(text))
      .subscribe((text) => {
        
        let html = this.editorElement.children[0].innerHTML;
        
        if (this.isEditorHtmlEmpty(html)) {
          html = null;
        }
        
        this.onModelChange(html);
        
        const textWidgetEditorResult: TextWidgetEditorResult = new TextWidgetEditorResult();
        textWidgetEditorResult.editor = this.quill;
        textWidgetEditorResult.html = html;
        textWidgetEditorResult.text = text;
        
        this.onContentChanged.emit(textWidgetEditorResult);
      });
  }
  
  /**
   * 에디터에 컨텐츠(텍스트) set
   * @param currentValue
   */
  private writeValue(currentValue: string): void {
    
    this.content = currentValue;
    
    if (!this.isEditorUndefined()) {
      if (this.content) {
        this.quill.pasteHTML(currentValue);
        return;
      }
      this.quill.setText('');
    }
  }
  
  /**
   * 툴바가 있으면 삭제
   */
  private hasToolbarIfRemove(): void {
    
    // 에디터에서 자동 생성된 툴바 엘리먼트를 선택한다
    const toolbar: Element = this.element.nativeElement.querySelector(this.TOOLBAR_CLASS_NAME);
    
    // 기존에 툴바 엘리먼트가 존재한다면 삭제 처리한다
    if (this.isToolbarNotEmpty(toolbar)) {
      toolbar.remove();
    }
  }
  
  /**
   * 툴바가 있으면
   * @param {Element} toolbar
   * @returns {boolean}
   */
  private isToolbarNotEmpty(toolbar: Element): boolean {
    return toolbar !== null;
  }
  
  /**
   * 에디터가 생성되지 않은 경우
   * @returns {boolean}
   */
  private isEditorUndefined(): boolean {
    return typeof this.quill === 'undefined';
  }
  
  /**
   * 에디터에 HTML 이 없다면
   * @param {string} html
   * @returns {boolean}
   */
  private isEditorHtmlEmpty(html: string): boolean {
    return html === this.EMPTY_HTML;
  }
  
  private onModelChange: Function = () => {
  };
  
  private onModelTouched: Function = () => {
  };
  
}
