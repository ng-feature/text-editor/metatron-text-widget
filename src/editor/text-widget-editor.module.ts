import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { COMPOSITION_BUFFER_MODE } from '@angular/forms';
import { TextWidgetEditorComponent } from './text-widget-editor.component';

@NgModule({
  imports: [],
  declarations: [
    TextWidgetEditorComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE, useValue: false
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    TextWidgetEditorComponent
  ]
})
export class TextWidgetEditorModule {
}
