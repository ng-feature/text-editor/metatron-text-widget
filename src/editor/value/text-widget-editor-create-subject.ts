export class TextWidgetEditorCreateSubject {
  public content: string;
  public modules: object;
  public placeholder: string;
  public readOnly: boolean;
}
