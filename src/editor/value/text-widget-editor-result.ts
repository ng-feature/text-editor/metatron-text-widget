export class TextWidgetEditorResult {
  editor: any;
  html: string;
  text: string;
}
