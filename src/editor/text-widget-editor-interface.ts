import { TextWidgetEditorResult } from './value/text-widget-editor-result';

export interface TextWidgetEditorInterface {
  
  /**
   * 초기화
   * @param {string} content
   * @param {Object} modules
   * @param {string} placeholder
   * @param {boolean} readOnly
   */
  initialize(content: string, modules: object, placeholder: string, readOnly: boolean): void;
  
  /**
   * 텍스트 넣기
   * @param {string} text
   */
  setContent(text: string): void;
  
  /**
   * 텍스트 가져오기
   * @returns {TextWidgetEditorResult}
   */
  getContent(): TextWidgetEditorResult;
  
}
