import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TextWidgetEditorModule } from '../editor/text-widget-editor.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TextWidgetEditorModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
