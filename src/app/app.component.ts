import { Component, OnInit, ViewChild } from '@angular/core';
import { TextWidgetEditorComponent } from '../editor/text-widget-editor.component';
import { TextWidgetEditorResult } from '../editor/value/text-widget-editor-result';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  // 텍스트 위젯 컴포넌트
  @ViewChild(TextWidgetEditorComponent)
  private textWidgetComponent: TextWidgetEditorComponent;

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Variables
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  // 화면에 표시 하기위한 값들
  public text: string = '';
  public html: string = '';
  public content: string = '';

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Constructor
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Override Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  public ngOnInit(): void {
    console.log('로딩 시작!');
    console.log('계속 재사용 하는 경우 OnInit 에서 무조건 초기화 실행!');
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize();
    this.textWidgetComponent.initialize('초기값?');
  }

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Public Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  // noinspection JSUnusedLocalSymbols
  public editorCreated($event: Event): void {
    console.log('텍스트 위젯 컴포넌트 생성완료');
    console.log('로딩바 종료!');
  }

  /**
   * 컨텐츠 변경 알림을 받아서 로그 출력
   * @param {TextWidgetResult} $event
   */
  public contentChanged($event: TextWidgetEditorResult): void {

    console.info('textWidgetResult', $event);

    const textWidgetResult: TextWidgetEditorResult = this.textWidgetComponent.getContent();
    this.text = typeof textWidgetResult.editor !== 'undefined' ? textWidgetResult.html : '';
    this.html = typeof textWidgetResult.editor !== 'undefined' ? textWidgetResult.html : '';
  }

  /**
   * 컨텐츠 ( 텍스트 ) 가져오기
   */
  public getText(): void {
    
    if (this.textWidgetComponent) {
      const textWidgetResult: TextWidgetEditorResult = this.textWidgetComponent.getContent();
      this.content = typeof textWidgetResult.editor !== 'undefined' ? textWidgetResult.html : '';
    }
  }

  /**
   * 컨텐츠 ( 텍스트 ) 주입
   */
  public setText(): void {

    if (this.textWidgetComponent) {
      this.textWidgetComponent.setContent('<p>AAAAA</p><p>BBBBB</p><p>CCCC</p><p>DDDD</p><p>EEEEE</p><p>FFFFF</p>');
    }
  }

  /**
   * 초기화
   */
  public initialize(): void {

    if (this.textWidgetComponent) {
      console.log('다시 초기화하는 시점에도 로딩바를 시작할 수 있다!!');
      console.log('initialize - 로딩 시작!');
      this.textWidgetComponent.initialize();
    }
  }

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Protected Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  | Private Method
  |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/


}
